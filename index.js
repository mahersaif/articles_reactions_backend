var express = require('express');
var path = require('path');
var mongoose = require('mongoose');

//'mongodb://localhost:27017/articlesReactions-testing';//process.env.DATABASE_URI ;
var databaseUri = process.env.DATABASE_URI ;
var DevelopmentDatabaseUri = process.env.Development_Database_Uri;

var app = express();
console.log("database URI: " + databaseUri);
mongoose.connect(databaseUri);
mongoose.Promise = Promise;

//Adding gzip compression
var compression = require('compression');
app.use(compression());

// adding CORS support
var cors = require('cors');
app.use(cors());

var bodyParser = require('body-parser');
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// // Serve static assets from the /public folder
app.use('/public', express.static(path.join(__dirname, '/public')));
var router = express.Router();

// var authentication = require('./source/Authentication');
// router.use(function (req, res, next) {
//     if(! authentication.isValidAccessToken(req)){
//         res.send({ error: "Invalide accesstoken" });
//     }else{
//         next()
//     }
// });

router.use('/getReactionsForArticle' , require('./source/GetReactionsForArticle'));
// router.use('/user' , require('./source/users'));
// router.use('/popular' , require('./source/popular'));
// router.use('/recommendations' , require('./source/Recommendations'));

router.use(function (req, res) {
  res.send({ error: "who got u here ?!" });
});

// router.use(function(err, req, res, next) {
//     res.status(err.status || 500);
//     res.render('error', {
//         message: "err.message",
//         error: {}
//     });
// });

app.use('/', router);

var port = process.env.PORT || 1337;

var server = require('http').createServer()
  , url = require('url')
  , port = port;

server.on('request', app);
server.listen(port, function () { console.log('Listening on ' + server.address().port) });
