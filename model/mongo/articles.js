var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// var User = require('./User')

var ArticlesSchema = new Schema({
  id: String,
  url : String,
  reactions: String,
  users_ids : String
}, {collection: 'articles'});

var articles = mongoose.model('articles', ArticlesSchema)
module.exports = articles;