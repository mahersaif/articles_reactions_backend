var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// var User = require('./User')

var ArticlesSchema = new Schema({
  id: String,
  url : String,
  reaction: String,
  user_id : String
}, {collection: 'articles_emotion'});

var articles = mongoose.model('article_emotion', ArticlesSchema)
module.exports = articles;