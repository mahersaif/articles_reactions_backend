var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
  _id: String,
  token: String,
  tokenType: String,
  userID : String,
  _updated_at : Date, 
  _created_at : Date
} , {collection: 'User'});

module.exports = mongoose.model('User', userSchema);