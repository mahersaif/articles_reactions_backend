var mongoose = require('mongoose');
var express = require('express');
var router = express.Router();
// var ProductAction = require('../models/ProductAction');

router.get('/updateUserID', function(req, res, next) {
    updateUserID(req, res);
});

function updateUserID(req, res) {
	console.log("Update UUID " + req.query.UUID + " with " + req.query.email)
	var conditions = { USER_ID: req.query.UUID }
	  , update = { $set: { USER_ID: req.query.email }}
	  , options = { multi: true };

	ProductAction.update(conditions, update, options, callback);
	function callback (err, numAffected) {
	 	if(err){
	 		console.log("Error while replacing UUID with email " + err);
	 		res.send(err);
	 	}else{
	 		res.status(200);
	 		res.send("Done");
	 	}
	}
}

module.exports = router;