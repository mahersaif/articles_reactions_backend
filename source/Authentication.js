var validTokens = ['c361e6277ea2150224225dcb0ce3aa623916afe2','iOS-Token'];
module.exports.isValidAccessToken = (req) => {
    return validTokens.includes(req.query.accesstoken);
}