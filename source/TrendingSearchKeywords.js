// var express = require('express');
// var router = express.Router();
// var _ = require('underscore');
// var NodeCache = require( "node-cache" );
// var cache = new NodeCache();

// var ActionWeights = require('./ActionWeights').actionWeights;

// const LANGUAGE = 'en';
// const COUNTRY = 'uae';
// const PAGE = 0;
// const LIMIT = 30;
// const HOURS = 72; 
// const CACHE_INTERVAL_MINS = 0.5;

// // var cachedResponseMap = ;
// cache.set("trendingCachedResponse" , new Object());

// router.get('/', function(req, res, next) {
//     var cachedResponseMap = cache.get("trendingCachedResponse");
//     if(cachedResponseMap[getKeyfromRequest(req)] && 
//         new Date().getTime() - cachedResponseMap[getKeyfromRequest(req)]['DATE'].getTime() < CACHE_INTERVAL_MINS*60*1000){
//             applyPagingAndRespond(req,res,cachedResponseMap[getKeyfromRequest(req)]['VALUE']);
//     }
//     else{
//         getDataFromQuery(req).then(responseObject => {
//             applyPagingAndRespond(req,res,responseObject);
//         });
//     }
// });

// function applyPagingAndRespond(req, res, responseObject) {
//         var limit = parseInt(req.query.limit) || LIMIT;
//         var page = parseInt(req.query.page) || PAGE;
//         responseObject['sortedProducts'] = responseObject.sortedProducts.slice(page * limit, (page + 1) * limit);
//         res.json(responseObject);
// }

// function getDataFromQuery(req) {
//         var responseObject = new Object();
//         return constructQuery(req).then(queryResults => {
//             var mapped = _.map(queryResults, result => {
//                 return new Object({_id : result.get('PRODUCT_ID'),
//                                     weight : ActionWeights[result.get('PRODUCT_ACTION')]});
//                 })    
//                 var vals = _.values(_.groupBy(mapped, productAction => productAction['_id']));
//                 var resultArray = new Array();
//                 _.each(vals, actionList => {
//                 resultArray.push (
//                     _.reduce(actionList, (actionItem1, actionItem2) => {
//                     actionItem2['weight'] += actionItem1['weight'];
//                     return actionItem2;
//                     })
//                 )
//                 })
//                 responseObject['sortedProducts'] = _.sortBy(resultArray, 'weight').reverse();
//                 return queryResults;
//         },
//         error => {
//             console.log("Error: " + error.code + " " + error.message);
//         }).then(queryResults => {
//             var keysTags = new Object();
//             var extractTagObject = (result, tagType) => {
//                 if(result.get(tagType) != null){
//                     if(keysTags[result.get(tagType)] != null){
//                         keysTags[result.get(tagType)].count++;
//                     }
//                     else{
//                         keysTags[result.get(tagType)] = new Object({
//                             type:tagType,
//                             value: result.get(tagType),
//                             count: 1
//                         });
//                     }
//                 }
//             };
//             _.each(queryResults, result => {
//                 extractTagObject(result,'BRAND');
//                 extractTagObject(result,'SHOP');
//                 extractTagObject(result,'CATEGORY');
//                 extractTagObject(result,'GENDER');
//             })
//             responseObject['keysTags'] = _.sortBy(_.values(keysTags),'count').reverse();
//         }, error => {
//             console.log("Error: " + error.code + " " + error.message);
//         }).then(() => {
//             //ADD TO CACHE
//             var cachedResponseMap = cache.get("trendingCachedResponse");
//             cachedResponseMap[getKeyfromRequest(req)] = new Object({
//                 DATE : new Date(),
//                 VALUE : responseObject
//             });
//             cache.set("trendingCachedResponse", cachedResponseMap);

//             return responseObject;
//         }, error => {
//             console.log("Error: " + error.code + " " + error.message);
//         })
// }

// function constructQuery(req) {
//     var query = new Parse.Query("ProductAction");
//     query.limit(1000);
//     var d = new Date();
//     var time = ((parseInt(req.query.hours) || HOURS) * 3600 * 1000);
//     var limitDate = new Date(d.getTime() - (time));
//     query.greaterThanOrEqualTo("createdAt", limitDate);
//     query.descending("createdAt");
//     query.equalTo("LANG", req.query.language || LANGUAGE);
//     query.equalTo("COUNTRY", req.query.country || COUNTRY);  
//     return query.find().then(results => {
//         if(req.query.categories || req.query.shops || req.query.brands || req.query.gender){
//             return _.filter(results, result => {
//                 if((req.query.categories &&
//                     (req.query.categories == result.get('CATEGORY') || 
//                     _.contains(req.query.categories, result.get('CATEGORY')))) ||
//                     (req.query.shops &&
//                     (req.query.shops == result.get('SHOP') || 
//                     _.contains(req.query.shops, result.get('SHOP')))) ||
//                     (req.query.brands &&
//                     (req.query.brands == result.get('BRAND') || 
//                     _.contains(req.query.brands, result.get('BRAND')))) ||
//                     (req.query.gender &&
//                     (req.query.gender == result.get('GENDER') || 
//                     _.contains(req.query.gender, result.get('GENDER'))))
//                 )
//                     return true;
//             })
//         }
//         else{
//             return results;
//         } 
//     });
// }

// function getKeyfromRequest(req) {
//     var key =  (req.query.language || LANGUAGE) +
//         (req.query.country || COUNTRY) + 
//         (req.query.country || COUNTRY) + 
//         (req.query.hours || HOURS);
//     if(req.query.categories) key += req.query.categories ;
//     if(req.query.shops) key += req.query.shops ;
//     if(req.query.brands) key += req.query.brands ;
//     if(req.query.gender) key += req.query.gender ;
//     return key;
// }

// module.exports = router;