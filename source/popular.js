var express = require('express');
var router = express.Router();
var _ = require('underscore');
var NodeCache = require( "node-cache" );
var cache = new NodeCache();


const ACTION_WEIGHT = require('./ActionWeights').actionWeights;
const LANGUAGE = 'en';
const COUNTRY = 'uae';
const PAGE = 0;
const LIMIT = 30;
const HOURS = 120; 
const CACHE_INTERVAL_MINS = 0;

// var cachedResponseMap = ;
cache.set("TopBrandsCachedResponse" , new Object());

router.get('/brands', function(req, res, next) {
    getResponse(req,res,'BRAND');
});

router.get('/shops', function(req, res, next) {
    getResponse(req,res,'SHOP');
});

router.get('/categories', function(req, res, next) {
    getResponse(req,res,'CATEGORY');
});

router.get('/offers', function(req, res, next) {
    getResponse(req,res,'offers');
});

function getResponse(req,res,type) {
    var cachedResponseMap = cache.get("TopBrandsCachedResponse");
    if(cachedResponseMap[getKeyfromRequest(req)] && 
        new Date().getTime() - cachedResponseMap[getKeyfromRequest(req)]['DATE'].getTime() < CACHE_INTERVAL_MINS*60*1000){
            applyPagingAndRespond(req,res,cachedResponseMap[getKeyfromRequest(req)]['VALUE']);
    }
    else{
        getDataFromQuery(req,type).then(responseObject => {
            console.log("RESULT");
            applyPagingAndRespond(req,res,responseObject);
        });
    }
}

function applyPagingAndRespond(req, res, responseObject) {
        var limit = parseInt(req.query.limit) || LIMIT;
        var page = parseInt(req.query.page) || PAGE;
        responseObject = responseObject.slice(page * limit, (page + 1) * limit);
        res.json(responseObject);
}

function getDataFromQuery(req, type){
    return getPopularData(req.query.hours, req.query.gender, req.query.country, req.query.language, type);
}

function getPopularData(hours, gender, country, language, type) {
        var responseObject = new Object();
        
        return executeQuery(hours, gender, country, language).then(queryResults => {
                var vals;
                if(type == 'offers'){
                    var mappedToWeight = _.map(queryResults, result => {
                            return new Object({_id : result.get('PRODUCT_ID'),
                                weight : result.get('SALE_RATIO') > 0 && result.get('SALE_RATIO') < 1 ? 
                                (ACTION_WEIGHT[result.get('PRODUCT_ACTION')] * result.get('SALE_RATIO') * 100 * (result.get('SALE_VALUE')))
                                 :0});
                    }) 
                    vals = _.values(_.groupBy(mappedToWeight, productAction => productAction['_id']));
                }
                else{
                    var mappedToWeight = _.map(queryResults, result => {
                            return new Object({'type' : result.get(type),
                                weight : ACTION_WEIGHT[result.get('PRODUCT_ACTION')]});
                    }) 
                    vals = _.values(_.groupBy(mappedToWeight, productAction => productAction['type']));
                }
                
                var resultArray = new Array();
                _.each(vals, actionList => {
                resultArray.push (
                    _.reduce(actionList, (actionItem1, actionItem2) => {
                    actionItem2['weight'] += actionItem1['weight'];
                    return actionItem2;
                    })
                )
                })

                responseObject = _.sortBy(resultArray, 'weight').reverse();
                responseObject = _.filter(responseObject, result =>{
                    return result.weight > 0;
                })
                return responseObject;
        },
        error => {
            console.log("Error: " + error.code + " " + error.message);
        })
        // .then(() => {
        //     var cachedResponseMap = cache.get("TopBrandsCachedResponse");
        //     cachedResponseMap[getKeyfromRequest(req)] = new Object({
        //         DATE : new Date(),
        //         VALUE : responseObject
        //     });
        //     cache.set("TopBrandsCachedResponse", cachedResponseMap);

        //     return responseObject;
        // }, error => {
        //     console.log("Error: " + error.code + " " + error.message);
        // })
}

function executeQuery(hours, gender, country, language){
    var d = new Date();
    var time = ((parseInt(hours) || HOURS) * 3600 * 1000);
    var limitDate = new Date(d.getTime() - (time));
    var query = new Parse.Query("ProductAction");
    query.limit(1000);
    query.greaterThanOrEqualTo("createdAt", limitDate);
    query.equalTo("LANG", language|| LANGUAGE);
    query.equalTo("COUNTRY", country || COUNTRY);  
    if(gender) query.equalTo("GENDER" , gender)
    return query.find();
}

function getKeyfromRequest(req) {
    var key =  (req.query.language || LANGUAGE) +
        (req.query.country || COUNTRY) + 
        (req.query.hours || HOURS);
    // if(req.query.categories) key += req.query.categories ;
    // if(req.query.shops) key += req.query.shops ;
    // if(req.query.brands) key += req.query.brands ;
    if(req.query.gender) key += req.query.gender ;
    return key;
}

module.exports.getPopularData = getPopularData;
module.exports = router;