var express = require('express');
var router = express.Router();
var _ = require('underscore');
var NodeCache = require( "node-cache" );
var cache = new NodeCache();
var mongoose = require('mongoose');
var apn = require("apn");

var path = require('path');

// var NotificationSubscriptions = require('../models/NotificationSubscriptions');
var Popular = require('./popular');
var User = require('../model/User');
var ProductAction = require('../model/ProductAction');

const ACTION_WEIGHT = require('./ActionWeights').actionWeights;
const LANGUAGE = 'en';
const COUNTRY = 'uae';
const PAGE = 0;
const LIMIT = 30;
const CACHE_INTERVAL_MINS = 0;

// var cachedResponseMap = ;
cache.set("TopBrandsCachedResponse" , new Object());

router.get('/getInterestsForUser', function(req, res, next) {
    getResponse(req,res);
});

router.get('/refreshAutomaticPushSubscriptions', function(req,res, next){
    // refreshAutomaticPushSubscriptions();
    refreshAutomaticPushSubscriptions().then(userInterests => {
        res.json(userInterests);
    } );
});

function getResponse(req,res) {
    var cachedResponseMap = cache.get("TopBrandsCachedResponse");
    if(cachedResponseMap[getKeyfromRequest(req)] && 
        new Date().getTime() - cachedResponseMap[getKeyfromRequest(req)]['DATE'].getTime() < CACHE_INTERVAL_MINS*60*1000){
            applyPagingAndRespond(req,res,cachedResponseMap[getKeyfromRequest(req)]['VALUE']);
    }
    else{
        getDataFromQuery(req).then(responseObject => {
            applyPagingAndRespond(req,res,responseObject);
        });
    }
}

function applyPagingAndRespond(req, res, responseObject) {
        // var limit = parseInt(req.query.limit) || LIMIT;
        // var page = parseInt(req.query.page) || PAGE;
        // responseObject = responseObject.slice(page * limit, (page + 1) * limit);
        res.json(responseObject);
}

function getUserInterests(userID){
    return constructQueryForUser(userID).then(queryResults => {
                var Recommendations = new Object();
                Recommendations['shops'] = extractSortedDataByField(queryResults ,'SHOP')
                Recommendations['brands'] = extractSortedDataByField(queryResults ,'BRAND')
                Recommendations['categories'] = extractSortedDataByField(queryResults ,'CATEGORY')
                Recommendations['gender'] = extractSortedDataByField(queryResults ,'GENDER')
                Recommendations['country'] = extractSortedDataByField(queryResults ,'COUNTRY')
                Recommendations['language'] = extractSortedDataByField(queryResults ,'LANG')
                return Recommendations
    }, error => {
        console.log("Error: " + error.code + " " + error.message);
    })
}

function getDataFromQuery(req) {
        var responseObject = new Object();
        var brands = new Array();
        return constructQuery(req).then(queryResults => {
                responseObject['shops'] = extractSortedDataByField(queryResults ,'SHOP')
                responseObject['brands'] = extractSortedDataByField(queryResults ,'BRAND')
                responseObject['categories'] = extractSortedDataByField(queryResults ,'CATEGORY')
                responseObject['gender'] = extractSortedDataByField(queryResults ,'GENDER')
                responseObject['country'] = extractSortedDataByField(queryResults ,'COUNTRY')
                responseObject['language'] = extractSortedDataByField(queryResults ,'LANG')
                return queryResults;
        },
        error => {
            console.log("Error: " + error.code + " " + error.message);
        }).then(() => {
            // var cachedResponseMap = cache.get("Recommendations");
            // cachedResponseMap[getKeyfromRequest(req)] = new Object({
            //     DATE : new Date(),
            //     VALUE : responseObject
            // });
            // cache.set("Recommendations", cachedResponseMap);

            return responseObject;
        }, error => {
            console.log("Error: " + error.code + " " + error.message);
        })
}

function extractSortedDataByField(queryResults , field){
    var mapped = _.map(queryResults, result => {
                return new Object({_id : result.get(field),
                                    weight : ACTION_WEIGHT[result.get('PRODUCT_ACTION')]});
                })    
    var resultsWithoutNull = _.filter(mapped, result => {
        if(result['_id'] != null){
            return result
        }
    })
     var vals = _.values(_.groupBy(resultsWithoutNull, brandsWithWeights => brandsWithWeights['_id']));
    var resultArray = new Array();
    _.each(vals, actionList => {
    resultArray.push (
                    _.reduce(actionList, (actionItem1, actionItem2) => {
                    actionItem2['weight'] += actionItem1['weight'];
                    return actionItem2;
                    })
                )
                })
                return _.sortBy(resultArray, 'weight').reverse();
}

function constructQuery(req) {
    return constructQueryForUser(req.query.userID)
}

function constructQueryForUser(userID){
        var query = new Parse.Query("ProductAction");
    query.limit(1000);
    var d = new Date();
    query.equalTo("USER_ID", userID);
    return query.find();
}

function getKeyfromRequest(req) {
    return req.query.userID;
}

function refreshAutomaticPushSubscriptions(){
    //1- TODO: get list of users
    var userID = 'ahmad.melegy@gmail.com';

    // Model.update({_id: id}, obj, {upsert: true, setDefaultsOnInsert: true}, cb);  
    
    // var notificationSubscription = NotificationSubscriptions({
    //     type: 'brand',
    //     value: 'Adidas',
    //     subscriptionType: 'Auto'
    // });

    // notificationSubscription.save(function(err) {
    //     if (err) throw err;
    //     console.log('User created!');
    // });

        // get all the users
    


    getUsersList().then( usersList => {
                    //Get Interests for each user
                    var userCount = 0;
                    function handleNewUser(){
                        userCount = userCount + 1;
                        if(userCount >= usersList.length){
                            console.log("ALL USERS ADDED - NOW SCAN FOR OFFERS");
                        }
                    }

                    var items = new Array();
                   console.log(usersList);
                   _.each(usersList, user => {
                       addSubscriptionsForUser(user).then(
                           userID => {
                               handleNewUser();
                           },
                           error => {
                               handleNewUser();
                           }
                       );
                   }) 
                },
                error => {
                    console.log('ERROR: ' + error);
                });



                //  _.each(userList, user => {
                //         getUserInterests(userID).then(recommendations =>{
                //         if(recommendations['shops']){
                //             subscriptionItems.push(); 
                //             var notificationSubscription = NotificationSubscriptions({
                //                 type: 'SHOP',
                //                 value: 'Adidas',
                //                 subscriptionType: 'Auto'
                //             }); 
                //             notificationSubscription.save(function(err) {
                //                 if (err) throw err;
                //                 console.log('User created!');
                //             });
                //         }
                //         return recommendations; 
                //     });
                //     });

    // User.find({})
    // .then(users => {
    //     console.log(users);
    // },
    // error => {

    // });

      //2- for each user, add entries for top subscriptions by brand, shop, category filterd by gender, country, laguage
    return getUserInterests(userID).then(recommendations =>{
        if(recommendations['shopss']){
           recommendations['shopss'] = _.first(recommendations['shops']) 
        }
        return recommendations; 
    });
}

function getUsersList(){
    return ProductAction.find({})
                .distinct('USER_ID');
}

//Addes user's intersts for a specific user to collection NotificationSubscriptions, 
// returns this operation as a promise 
function addSubscriptionsForUser(userID){

            return getUserInterests(userID).then(recommendations =>{

                var shop = _.first(recommendations['shops']);
                var brand = _.first(recommendations['brands']);
                var category = _.first(recommendations['categories']);
                var language = _.first(recommendations['language']);
                var country = _.first(recommendations['country']);
                var gender =  _.first(recommendations['gender']);
            
            if(recommendations['shops']){
            var notificationForShop = NotificationSubscriptions({
                                type: 'SHOP',
                                value: shop['_id'],
                                subscriptionType: 'Auto',
                                userID: userID,
                                lang : language['_id'],
                                country: country['_id'],
                                gender : gender['_id']
                            });

            var notificationForBrand = NotificationSubscriptions({
                                type: 'BRAND',
                                value: brand['_id'],
                                subscriptionType: 'Auto',
                                userID: userID,
                                lang : language['_id'],
                                country: country['_id'],
                                gender : gender['_id']
                            });

            var notificationForCategory = NotificationSubscriptions({
                                type: 'CATEGORY',
                                value: category['_id'],
                                subscriptionType: 'Auto',
                                userID: userID,
                                lang : language['_id'],
                                country: country['_id'],
                                gender : gender['_id']
                            });

            notificationForShop.save();
            notificationForBrand.save();
            notificationForCategory.save();
            }

            return userID;
        },
        error => {
            console.log("ERROR IN USER : " + userID + " INTERESTS");
        });
}

function sendPushNotification(){
    var tokens = ["e0a4251aba08b473baa2ffe01f1a29ef610058ab1105d6f64baf68a9e51c1d35"];

var service = new apn.Provider({
  cert: path.join(__dirname, "/certificates/cert.pem"),
  key: path.join(__dirname, "/certificates/key.pem"),
});

var note = new apn.Notification({
	alert:  "Breaking News: I just sent my first Push Notification",
});

// The topic is usually the bundle identifier of your application.
note.topic = "com.shopshopme.app";

console.log(`Sending: ${note.compile()} to ${tokens}`);
service.send(note, tokens).then( result => {
    console.log("sent:", result.sent.length);
    console.log("failed:", result.failed.length);
    console.log(result.failed);
});
}




module.exports = router;