var express = require('express');
var router = express.Router();
var _ = require('underscore');
var mongoose = require('mongoose');
// const decode = require('normalize-url');
var decode = require('urldecode')

var Reactions = require('../model/Reactions').reactions;
var articles = require('../model/mongo/articles');
var article_emotion = require('../model/mongo/article_emotion');

router.get('/', function(req, res, next) {

    var articleId = req.query.page_url;
    var articleUrl = req.query.page_url;
    var articleUserID = req.query.uuid;

    if(articleId){
        getArticleVoteResults(decode(articleUrl)).then(voteResult => {
            res.json(createVoteResultsResponse(voteResult));
        });
    }
    else{
        res.status(400).send(new Object({ error: "Invalid article URL" }));
    }
});

router.get('/reactionForArticle',function(req, res, next) {

    var articleId = req.query.page_url;
    var articleUrl = req.query.page_url;
    var articleUserID = req.query.uuid;

    if(!articleId){
        res.status(400).send(new Object({ error: "Invalid article URL" }));
    }
    else if(!articleUserID){
        res.status(400).send(new Object({ error: "Invalid user ID" }));
    }
    else{
        getReactionForUser(decode(articleId) , articleUserID).then(reaction => {
            res.json(new Object({result : reaction}));
        });
    }
});

router.post('/' , function(req, res, next) {
    var articleId = req.body.page_url;
    var articleUrl = req.body.page_url;
    var articleUserID = req.body.uuid;
    var reaction = req.body.reaction;

    if(!articleUrl){
        res.status(400).send(new Object({ error: "Invalid article URL" }));
    }
    else if(!articleUserID){
        res.status(400).send(new Object({ error: "Invalid user ID" }));
    }
    else if(! _.contains(Reactions,reaction)){
        res.status(400).send(new Object({ error: "Invalid reaction :" + reaction}));
    }
    else{
        //first add the reaction, then respond with vote result
        addReactionToArticle(decode(articleId),articleUserID, reaction).then(success => {
            if(success){
                getArticleVoteResults(decode(articleUrl)).then(voteResult => {
                    res.json(createVoteResultsResponse(voteResult));
                    res.end();
                });
            }
            else{
                res.status(400).send(new Object({ error: "user already voted" }));
                res.end();
            }
        })
    }
});

function createVoteResultsResponse(voteResult){
    var results = new Object();
    var reactions = new Array();
    _.each(_.keys(voteResult), key => {
        var reaction = new Object();
        reaction['count']= voteResult[key];
        reaction['reaction'] = key;
        reactions.push(reaction);
    })
    results['reactions'] = reactions;
    return results;
}


//return true if vote is added, return false if user voted before (as a promise)
function addReactionToArticle(articleId, userId, userReaction){
    return article_emotion.findOne({'id' : articleId, 'user_id' : userId })
    .then(result => {
        if(!result){ //user didn't vote before, add his vote to db
            var articleReaction =  article_emotion({id : articleId,
            reaction: userReaction,
            user_id: userId});

            // return articleReaction.save(error => {
            //     console.log(error);
            //     return false;
            // });
            return articleReaction.save().then(result => {
                return true;
            }, error => {
                return false;
            });
            // return true;
        }
        else{ //user voted before
            return false;
        }
    }, error => {
        console.log(error);
        return false;
    });
}

//get all reactions for article and create voteResult object
function getArticleVoteResults(articleId){
    return article_emotion.find({'id':articleId}).then(results => {

        //Create empty vote count
        var voteResults = new Object();
        _.each(Reactions, reaction => {
                voteResults[reaction] = 0;
        });

        //add each vote to voteReults object
        _.each(results, userReaction => {
            // voteResults['votes_count']++;
            voteResults[userReaction.reaction]++;
        })
        return voteResults;
    }, error =>{
        console.log(error);
    });
}


function getReactionForUser(articleId , articleUserID){
    return article_emotion.findOne({'id' : articleId, 'user_id' : articleUserID })
    .then(result => {
        if(!result){ //user didn't vote before, add his vote to db
            return 'NA';
        }
        else{ //user voted before
            return result.reaction;
        }
    }, error => {
        console.log(error);
        return false;
    });
}
// //Get article if already exist or create new entry if not exist
// function getOrCreateArticle(articleId, articleUrl, userId){

//     var article;

//     return articles.findOne({'id' : articleId}).then(result => {

//         if(!result){  //Article doesn't exist, create it
//             var emptyReactions = new Object();
//             _.each(Reactions, reaction => {
//                 emptyReactions[reaction] = 0
//             });

//             article = articles({
//                 id : articleId,
//                 url : articleUrl,
//                 reactions: JSON.stringify(emptyReactions),
//                 user_id : userId
//             });
//             article.save(error => {
//                 console.log('error saving article ' + error);
//             });
//             console.log('adding new article ' + article);
//         }
//         else{
//             article = result;
//             console.log('get existing article ' + article);
//         }

//         return article;

//     }, error => {
//         //TODO: Logging
//         console.log('error : ' + error);
//     });
// }

module.exports = router;
